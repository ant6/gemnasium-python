package main

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"
	scannercli "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/cli"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/pipdeptree"
)

func analyzeFlags() []cli.Flag {
	return scannercli.ClientFlags()
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	// parse args and make new client
	client, err := scannercli.NewClient(c)
	if err != nil {
		return nil, err
	}

	// find compatible file
	filename, err := findFile(path)
	if err != nil {
		return nil, err
	}

	// install dependencies
	cmd := exec.Command("pip", "install", "-r", filename)
	cmd.Dir = path
	cmd.Env = os.Environ()
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdout
	if err := cmd.Run(); err != nil {
		return nil, err
	}

	// run pipdeptree
	cmd = exec.Command("pipdeptree", "--json")
	cmd.Dir = path
	cmd.Env = os.Environ()
	cmd.Stderr = os.Stderr

	// pipdeptree output is the parser input
	input, err := cmd.Output()
	if err != nil {
		return nil, err
	}

	// parse input, get parsed file
	buf := bytes.NewBuffer(input)
	source, err := scanner.Parse(buf, "pipdeptree.json", filename)
	if err != nil {
		return nil, err
	}

	// get advisories
	advisories, err := client.Advisories(source.Packages())
	if err != nil {
		return nil, err
	}

	// return affected sources
	var output bytes.Buffer
	result := scanner.AffectedSources(advisories, *source)
	enc := json.NewEncoder(&output)
	enc.SetIndent("", "  ")
	if err := enc.Encode(result); err != nil {
		return nil, err
	}

	return ioutil.NopCloser(&output), nil
}

// ErrNoCompatibleFile is raised when there's no compatible file in the directory.
type ErrNoCompatibleFile struct {
	Path string
}

// Error returns the error message.
func (e ErrNoCompatibleFile) Error() string {
	return "No compatible file in " + e.Path
}

// findFile returns the basename of a compatible file found in given directory.
func findFile(path string) (string, error) {
	for _, filename := range []string{"requirements.txt", "requirements.pip", "Pipfile", "requires.txt"} {
		if _, err := os.Stat(filepath.Join(path, filename)); err == nil {
			return filename, nil
		}
	}
	return "", ErrNoCompatibleFile{path}
}
